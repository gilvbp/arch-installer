#!/bin/sh

ln -sf /usr/share/zoneinfo/Europe/Prague /etc/localtime
hwclock --systohc
echo "cs_CZ.UTF-8 UTF-8" >> /etc/locale.gen
echo "en_US.utf8 UTF-8" >> /etc/locale.gen
locale-gen
echo "LC_COLLATE=C" > /etc/locale.conf
echo "LANG=cs_CZ.UTF-8" >> /etc/locale.conf
echo "LANGUAGE=cs_CZ" >> /etc/locale.conf
echo "LC_TIME=cs_CZ.UTF-8" >> /etc/locale.conf
echo "KEYMAP=cz-us-qwertz" > /etc/vconsole.conf
echo "${10}" > /etc/hostname
echo 'ALL_config="/etc/mkinitcpio.conf"' > /etc/mkinitcpio.d/linux-lts.preset
echo "ALL_kver=\"/boot/vmlinuz-${3}\"" >> /etc/mkinitcpio.d/linux-lts.preset
echo "PRESETS=('default' 'fallback')" >> /etc/mkinitcpio.d/linux-lts.preset
echo "default_image=\"/boot/initramfs-${3}.img\"" >> /etc/mkinitcpio.d/linux-lts.preset
echo "fallback_image=\"/boot/initramfs-${3}-fallback.img\"" >> /etc/mkinitcpio.d/linux-lts.preset
echo 'fallback_options="-S autodetect"' >> /etc/mkinitcpio.d/linux-lts.preset
cp /etc/mkinitcpio.d/linux-lts.preset /etc/mkinitcpio.d/linux.preset -rf
echo "[multilib]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf


pacman --noconfirm -Syu

if [ "${9}" = "nvidia" ]; then
    pacman  --noconfirm -S lib32-opencl-nvidia lib32-nvidia-utils nvidia-lts opencl-nvidia libvdpau lib32-libvdpau
elif [ "${9}" = "amd" ]; then
    pacman  --noconfirm -S mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau ib32-mesa-vdpau radeontop
elif [ "${9}" = "intel" ]; then
    pacman  --noconfirm -S vulkan-intel libva-intel-driver xf86-video-intel
fi

if [ "${14}" = "amd" ]; then
    pacman --noconfirm -S amd-ucode
fi

pacman --noconfirm -S zsh openssh openssl chromium firefox networkmanager wget nano code python \
    pip nmap htop ecryptfs-utils wayland rsync lsof dialog gstreamer steam archlinux-keyring \
    git firewalld imagemagick cpupower ntfs-3g unrar zip unzip openvpn networkmanager-openvpn \
    docker docker-compose nodejs flac tor nyx certbot clang jdk-openjdk net-tools picocom sshfs \
    python-pyudev blender gimp inkscape jq hwinfo bc expac dpkg bluez bluez-utils npm jq \
    gendesk cronie pacman-contrib android-tools noto-fonts noto-fonts-emoji ttf-opensans \
    ttf-fira-sans ttf-linux-libertine ttf-dejavu ttf-croscore dfu-util gperf esptool \
    arduino arduino-cli


npm config set ignore-scripts true
npm install -g prettier typescript vercel eslint jshint
pip install pylint flake8 mypy jedi

sed -i "s/-march=x86-64 -mtune=generic/-march=native/" /etc/makepkg.conf


groupadd plugdev
echo "KERNEL==\"hidraw*\", SUBSYSTEM==\"hidraw\", MODE=\"0664\", GROUP=\"plugdev\"" > /etc/udev/rules.d/99-hidraw.rules
useradd -m --groups wheel,audio,disk,input,kvm,optical,scanner,storage,video,plugdev -s /bin/zsh ${2}
echo "Seting user password:"
if [ ! -z "${6}" ]; then
    echo "${2}:${6}" | chpasswd
else
    passwd ${2}
fi

mv /etc/sudoers /etc/sudoers2
echo "%wheel ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers
su ${2} -c "git clone https://aur.archlinux.org/yay.git"
su ${2} -c "cd ./yay"
su ${2} -c "makepkg -sri --noconfirm"
rm ./yay -rf

if [ "${8}" = "gnome" ]; then
    pacman --noconfirm -S gnome-shell xorg-server-xwayland egl-wayland libinput \
    network-manager-applet  gnome-calendar mplayer ipset nemo \
    file-roller eog gdm gnome-screenshot tilix gvfs-google gvfs-goa gvfs-gphoto2 gvfs \
    gvfs-mtp gnome-keyring solaar gnome-shell-extensions dconf gnome-tweaks dconf-editor \
    gnome-mplayer gmtk gparted gnome-text-editor
    su ${2} -c "yay --mflags --skipinteg --cleanafter --removemake  --noconfirm -S mutter-vrr"
    su ${2} -c "yay --mflags --skipinteg --cleanafter --removemake  --noconfirm -S gnome-control-center-vrr"
    systemctl enable gdm.service

    cp /conf/gnome/tilix_quake.sh /etc/tilix_quake.sh

    cp -rf /conf/gnome/icons /home/${2}/.icons
    cp -rf /conf/gnome/themes /home/${2}/.themes
    mkdir -p /home/${2}/.config/gtk-4.0/
    cp -rf /conf/gnome/themes/Flat-Remix-LibAdwaita-Blue-Dark-Solid/* /home/${2}/.config/gtk-4.0/
    mkdir /home/${2}/.local
    mkdir /home/${2}/.local/share
    mkdir /home/${2}/.local/share/gnome-shell
    mkdir /home/${2}/.config

    cp -rf /conf/gnome/extensions /home/${2}/.local/share/gnome-shell/extensions

    chown ${2} /home/${2}/.local -R
    chmod u+rw /home/${2}/.local -R
    chown ${2} /home/${2}/.config -R
    chmod u+rw /home/${2}/.config -R
    chown ${2} /home/${2}/.icons -R
    chmod u+rw /home/${2}/.icons -R
    chown ${2} /home/${2}/.themes -R
    chmod u+rw /home/${2}/.themes -R

    cp -rf /conf/gnome/firstrun.sh /first_boot.sh
    mkdir -p /home/${2}/.config/autostart
    cp -rf /conf/gnome/first_boot-autostart.desktop /home/${2}/.config/autostart/first_boot-autostart.desktop
    chown ${2} /first_boot.sh
    chown ${2} /home/${2}/.config/autostart/first_boot-autostart.desktop
    chmod +x /first_boot.sh

    cp /conf/gnome/gnome.conf /home/${2}/.gnome.conf
    chown ${2} /home/${2}/.gnome.conf

    sed "/^\[daemon\]$/a AutomaticLogin=${2}" /etc/gdm/custom.conf > /buffer
    mv ./buffer /etc/gdm/custom.conf
    sed "/^\[daemon\]$/a AutomaticLoginEnable=True" /etc/gdm/custom.conf > /buffer
    mv ./buffer /etc/gdm/custom.conf

elif [ "${8}" = "sway" ]; then
	pacman --noconfirm -S sway swaybg swayidle swaylock foot tilix light alsa-utils grim \
            otf-font-awesome vimiv nemo fzf xorg-xwayland pulseaudio file-roller

    ## install my forked waybar
    pacman --noconfirm -S meson ninja catch2 cmake scdoc wayland-protocols fmt gtk-layer-shell \
        gtkmm3 libappindicator-gtk3 chrono-date libdbusmenu-gtk3 libevdev jsoncpp libmpdclient \
        libnl libpulse libsigc++ spdlog sndio upower libxkbcommon
    cd /tmp
    git clone https://github.com/katry0/Waybar.git
    cd Waybar
    meson build
    ninja -C build
    ninja -C build install
    cd /

    cp -rf /conf/gnome/icons /home/${2}/.icons
    cp -rf /conf/gnome/themes /home/${2}/.themes
    mkdir -p /home/${2}/.icons/default
    echo "[icon theme]" > /home/${2}/.icons/default/index.theme
    echo "Inherits=Obsidian" >> /home/${2}/.icons/default/index.theme
    mkdir -p /home/${2}/.config/gtk-3.0/settings.ini
    echo "[Settings]" > /home/${2}/.config/gtk-3.0/settings.ini
    echo "gtk-application-prefer-dark-theme=1" >> /home/${2}/.config/gtk-3.0/settings.ini
    echo "gtk-cursor-theme-name=Obsidian" >> /home/${2}/.config/gtk-3.0/settings.ini

    mkdir /home/${2}/.config
    mkdir /home/${2}/.config/sway
    mv /conf/sway/firs_boot.sh /first_boot.sh
    cp /conf/sway/* /home/${2}/.config/sway/
    mv /home/${2}/.config/sway/waybar /home/${2}/.config/waybar
    mv /home/${2}/.config/sway/swaylock /home/${2}/.waybar
    wget https://raw.githubusercontent.com/Biont/sway-launcher-desktop/master/sway-launcher-desktop.sh -O /home/${2}/.config/sway/sway-launcher-desktop.sh
    echo "\nrm /tmp/.menu-open\n" >> /home/${2}/.config/sway/sway-launcher-desktop.sh
    if [[ "$(hwinfo | grep eDP -m1)" != "" ]]; then
        monitor=$(hwinfo | grep " Monitor" -A 13 -m 1)
        res_x=$(echo $monitor | \grep "Resolution" | sed "s/  Resolution: //" | sed -r "s/@.*//" | sed -r "s/x.*//")
        res_y=$(echo $monitor | \grep "Resolution" | sed "s/  Resolution: //" | sed -r "s/@.*//" | sed -r "s/.*x//")
        size_x=$(echo $monitor | \grep Size | sed "s/  Size: //" | sed "s/ mm//" | sed -r "s/x.*//")
        size_y=$(echo $monitor | \grep Size | sed "s/  Size: //" | sed "s/ mm//" | sed -r "s/.*x//")
        scale=$(bc <<< "scale=2; $res_x / $size_x / 7.5")
        PROPS="output eDP-1 pos 0 0 res ${res_x}x${res_y}\noutput eDP-1 scale ${scale}\n"
        sed "s/#__INTERNAL_DISPLAY_CONFIG__/$PROPS/"
    fi
    localectl set-x11-keymap cz,us
    echo "exec foot -e /first_boot.sh" >> /home/${2}/.config/sway/config

    chown ${2} /home/${2}/.config -R
    chown ${2} /home/${2}/.swaylock -R
    chown ${2} /home/${2}/.icons -R
    chown ${2} /home/${2}/.themes -R
    chown ${2} /first_boot.sh
    chmod +x /first_boot.sh
fi

cp -rf /conf/bashrc /home/${2}/.bashrc
cp -rf /conf/zshrc /home/${2}/.zshrc
cp -rf /conf/profile /home/${2}/.profile
cp -rf /conf/inputrc /home/${2}/.inputrc


mkdir /home/${2}/.zsh
wget https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.zsh -O /home/${2}/.zsh/_git


chown ${2} /home/${2}/.bashrc
chown ${2} /home/${2}/.zshrc
chown ${2} /home/${2}/.zsh
chown ${2} /home/${2}/.zsh/_git
chown ${2} /home/${2}/.profile
chown ${2} /home/${2}/.inputrc


chown ${2} ~/home/${2}/.bashrc
chown ${2} ~/home/${2}/.profile
chown ${2} ~/home/${2}/.inputrc

cp -f /conf/bashrc /root/.bashrc
cp -f /conf/profile /root/.profile
cp -f /conf/dircolors /root/.dircolors

systemctl enable NetworkManager
systemctl enable docker.service
systemctl enable firewalld.service
systemctl enable bluetooth.service
systemctl enable cronie.service
systemctl enable tor.service
systemctl enable sshd.service
mkdir /home/${2}/.ssh
echo "AddKeysToAgent yes" >> /home/${2}/.ssh/config
echo "ForwardAgent yes" >> /home/${2}/.ssh/config
echo "" >> /home/${2}/.ssh/config
chown ${2} /home/${2}/.ssh -R

gpasswd -a ${2} docker
chmod +x /loged.sh
echo "Setting root password:"
if [ ! -z "${7}" ]; then
    echo "root:${7}" | chpasswd
else
    passwd
fi

gpasswd -a ${2} adbusers
gpasswd -a ${2} plugdev
gpasswd -a ${2} tor
cp -rf /conf/android.rules /etc/udev/rules.d/51-android.rules

su ${2} -c "/loged.sh ${8} ${2} ${13} ${14} ${15}"

if [ "${8}" = "sway" ]; then
    echo "sway" > /etc/greetd/environments
    echo "bash" >> /etc/greetd/environments

    cp /home/${2}/.themes/Flat-Remix-GTK-Blue-Dark/gtk-3.0/gtk.css /etc/greetd/gtk.css
    echo "exec \"gtkgreet -l -s /etc/greetd/gtk.css; swaymsg exit\"" > /etc/greetd/sway-config
    echo "bindsym Mod4+shift+e exec swaynag \\" >> /etc/greetd/sway-config
    echo "-t warning \\" >> /etc/greetd/sway-config
    echo "-m 'What do you want to do?' \\" >> /etc/greetd/sway-config
    echo "-b 'Poweroff' 'systemctl poweroff' \\" >> /etc/greetd/sway-config
    echo "-b 'Reboot' 'systemctl reboot'" >> /etc/greetd/sway-config
    echo "input type:keyboard {" >> /etc/greetd/sway-config
    echo "xkb_layout cz,us" >> /etc/greetd/sway-config
    echo "}" >> /etc/greetd/sway-config
    echo "focus_follows_mouse no" >> /etc/greetd/sway-config
    echo "input type:touchpad {" >> /etc/greetd/sway-config
    echo "tap enabled" >> /etc/greetd/sway-config
    echo "natural_scroll enabled" >> /etc/greetd/sway-config
    echo "}" >> /etc/greetd/sway-config
    echo "include /etc/sway/config.d/*" >> /etc/greetd/sway-config

    echo "[terminal]" > /etc/greetd/config.toml
    echo "vt = 1" >> /etc/greetd/config.toml
    echo "[default_session]" >> /etc/greetd/config.toml
    echo "command = \"sway --config /etc/greetd/sway-config\"" >> /etc/greetd/config.toml
    echo "user = \"greeter\"" >> /etc/greetd/config.toml
    systemctl enable greetd.service
elif [ "${8}" = "gnome" ]; then
    echo ""
else
    echo "%wheel ALL=(ALL) ALL" > ./sudoers
fi

if [ "${11}" = "YES" ]; then
    pacman --noconfirm -S intel-ucode amd-ucode grub
    pacman --noconfirm -S mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon libva-mesa-driver lib32-libva-mesa-driver mesa-vdpau \
    lib32-mesa-vdpau xf86-video-amdgpu vulkan-intel libva-intel-driver xf86-video-intel lib32-opencl-nvidia lib32-nvidia-utils \
    nvidia-lts opencl-nvidia libvdpau lib32-libvdpau
    cp -f /conf/mkinitcpio.conf /etc/mkinitcpio.conf
    mkinitcpio -p linux
    if [ ! -z "${12}" ]; then
        grub-install --target=i386-pc ${12} --recheck
    fi
    grub-install --target=x86_64-efi --efi-directory=/boot --removable --recheck
    grub-mkconfig -o /boot/grub/grub.cfg
else
    if [ "${4}" != "NO" ]; then
        bootctl --path=/boot install
        echo "bootloader installed"
        ls /boot
        echo "default arch${3}" > /boot/loader/loader.conf
        echo "timeout 4" >> /boot/loader/loader.conf
        echo "console-mode max" >> /boot/loader/loader.conf
    fi
    blkid -s PARTUUID | grep ${1} | tr "\"" "\n" | grep "-" > ./buff.txt
    A=`cat buff.txt`
    if [ "${14}" = "amd" != "NO" ]; then
        export AMD_P_STATE="amd_pstate=passive"
    else
        export AMD_P_STATE=""
    fi
    echo "title	ArchLinux" > /boot/loader/entries/arch${3}.conf
    echo "linux	/vmlinuz-${3}" >> /boot/loader/entries/arch${3}.conf
    echo "initrd	/initramfs-${3}.img" >> /boot/loader/entries/arch${3}.conf
    echo "options root=PARTUUID=${A} rw quiet splash ${AMD_P_STATE}"  >> /boot/loader/entries/arch${3}.conf
    rm -f ./buff.txt
    mkinitcpio -p linux
    if [ ! -z "${16}" ]; then
        mkdir /tmp/winefi
        mount ${16} /tmp/winefi
        cp -r /tmp/winefi/EFI/Microsoft /boot/EFI/
        umount ${16}
    fi
fi

paccache -ruk0
rm -rf /var/log/*
rm -rf /var/cache/*
rm -rf /var/lib/systemd/coredump/*

# serial devices user access rights:

# esp32-c3
echo "ATTRS{idVendor}==\"1a86\", ATTRS{idProduct}==\"7523\", SYMLINK+=\"ttyUSB0\", GROUP=\"${2}\", MODE=\"0660\"" >> /etc/udev/rules.d/50-usb-serial.rules

# raspberry pi pico
echo "ATTRS{idVendor}==\"2e8a\", ATTRS{idProduct}==\"0005\", SYMLINK+=\"ttyACM0\", GROUP=\"${2}\", MODE=\"0660\"" >> /etc/udev/rules.d/50-usb-serial.rules

# HOME directory encrypt preparation
# modprobe ecryptfs
# ecryptfs-migrate-home -u {2}
# echo -e "ls" /empty.sh
# chmod a+x /empty.sh
# su ${2} -c /empty.sh

# sed -i "/^auth *\[default=die\] *pam_faillock.so *authfail/aauth       [success=1 default=ignore]  pam_succeed_if.so service = systemd-user quiet\nauth       required                    pam_ecryptfs.so unwrap" /etc/pam.d/system-auth
# sed -i "/^-password/apassword   optional                    pam_ecryptfs.so" /etc/pam.d/system-auth
# sed -i "/^session *required *pam.unix.so/asession    [success=1 default=ignore]  pam_succeed_if.so service = systemd-user quiet\nsession    optional                    pam_ecryptfs.so unwrap" /etc/pam.d/system-auth
# su ${2} -c /empty.sh
# rm /empty.sh
# rm -rf /home/${2}.*

echo "Installation DONE!"
