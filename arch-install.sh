#!/bin/sh

ARGUMENT_LIST=(
    "root-partition"
    "boot-partition"
    "home-partition"
    "user"
    "user-password"
    "root-password"
    "kernel-name"
    "portable"
    "format-boot"
    "format-home"
    "desktop"
    "gpu"
    "hostname"
    "skipformat"
    "cpu"
    "zen-version"
    "user-email"
    "win-efi-partition"
)
opts=$(getopt \
    --longoptions "$(printf "%s:," "${ARGUMENT_LIST[@]}")" \
    --name "$(basename "$0")" \
    --options "" \
    -- "$@"
)
eval set --$opts
while [[ $# -gt 0 ]]; do
    case $1 in
        --root-partition)
            rootPartition=$2
            shift 2
            ;;

        --boot-partition)
            bootPartition=$2
            shift 2
            ;;

        --home-partition)
            homePartition=$2
            shift 2
            ;;

        --win-efi-partition)
            winEfiPartition=$2
            shift 2
            ;;

        --user)
            user=$2
            shift 2
            ;;

        --user-password)
            userPassword=$2
            shift 2
            ;;

        --user-email)
            email=$2
            shift 2
            ;;

        --root-password)
            rootPassword=$2
            shift 2
            ;;

        --kernel-name)
            kernelName=$2
            shift 2
            ;;

        --portable)
            portable=YES
            shift
            ;;

    	--format-boot)
                formatBoot=YES
                shift
                ;;

    	--format-home)
                formatHome=YES
                shift
                ;;
        --desktop)
                desktop=$2
                shift 2
                ;;
        --gpu)
                gpu=$2
                shift 2
                ;;
        --cpu)
                cpu=$2
                shift 2
                ;;
        --zen-version)
                zen=$2
                shift 2
                ;;
        --hostname)
                hostname=$2
                shift 2
                ;;
        --device)
                device=$2
                shift 2
                ;;
        --skip-format)
                skipFormat=YES
                shift 2
                ;;
        *)
            break
            ;;
    esac
done


if [ -z "${rootPartition}" ] || [ -z "${bootPartition}" ] || [ -z "${user}" ]; then
	echo "Must define --boot-partition, --root-partition and --user"
	exit
fi
if [ -z "${email}" ]; then
    echo "Define --user-email parameter"
    exit
fi
if [ -z "${kernelName}" ]; then
	kernelName="linux-lts"
fi
if [ -z "${hostname}" ]; then
	hostname="computer"
fi
if [ -z "${cpu}" ]; then
	cpu="any"
fi
if [ -z "${zen}" ]; then
	zen="2"
fi
if [ -z "${portable}" ]; then
	portable="NO"
    device="NO"
fi

loadkeys cz-qwertz
timedatectl set-ntp true
umount ${bootPartition}
if [ ! -z "${homePartition}" ]; then
    umount ${homePartition}
fi
if [ ! -z "${homePartition}" ]; then
    umount ${homePartition}
fi
umount ${rootPartition}

if [ ! -z "${formatBoot}" ]; then
    mkfs.fat -F32 ${bootPartition}
else
    formatBoot = "NO"
fi
if [ -z "${skipFormat}" ]; then
	mkfs.ext4 -F ${rootPartition}
fi
if [ ! -z "${homePartition}" ] && [ ! -z "${formatHome}" ]; then
    mkfs.ext4 -F ${homePartition}
fi
mount ${rootPartition} /mnt
mkdir /mnt/boot
mkdir /mnt/home
if [ ! -z "${homePartition}" ]; then
    mount ${homePartition} /mnt/home
fi
mkdir /mnt/etc
mkdir /mnt/etc/mkinitcpio.d/
echo 'ALL_config="/etc/mkinitcpio.conf"' > /mnt/etc/mkinitcpio.d/linux-lts.preset
echo 'ALL_kver="/boot/vmlinuz-'${kernelName}'"' >> /mnt/etc/mkinitcpio.d/linux-lts.preset
echo "PRESETS=('default' 'fallback')" >> /mnt/etc/mkinitcpio.d/linux-lts.preset
echo 'default_image="/boot/initramfs-'${kernelName}'.img"' >> /mnt/etc/mkinitcpio.d/linux-lts.preset
echo 'fallback_image="/boot/initramfs-'${kernelName}'-fallback.img"' >> /mnt/etc/mkinitcpio.d/linux-lts.preset
echo 'fallback_options="-S autodetect"' >> /mnt/etc/mkinitcpio.d/linux-lts.preset
cp /mnt/etc/mkinitcpio.d/linux-lts.preset /mnt/etc/mkinitcpio.d/linux.preset -rf

pacman --noconfirm -Sy archlinux-keyring
LC_ALL="cs_CZ.UTF-8" pacstrap /mnt bash bzip2 coreutils cryptsetup device-mapper dhcpcd diffutils e2fsprogs file filesystem findutils gawk \
gcc-libs gettext glibc grep gzip inetutils iproute2 iputils jfsutils less licenses linux-lts linux-firmware logrotate lvm2 \
man-db mdadm nano netctl pacman pciutils perl procps-ng psmisc reiserfsprogs s-nail sed shadow sysfsutils \
systemd-sysvcompat tar texinfo usbutils util-linux vi which xfsprogs base-devel linux-lts-headers
mkdir /mnt/tempboot
mv /mnt/boot/* /mnt/tempboot
mount ${bootPartition} /mnt/boot
mv /mnt/tempboot/vmlinuz-linux-lts /mnt/boot/vmlinuz-${kernelName}
mv /mnt/tempboot/* /mnt/boot/
rmdir /mnt/tempboot
genfstab -U /mnt >> /mnt/etc/fstab
cp ./loged.sh /mnt/loged.sh
chmod +x ./chrooted.sh
cp ./chrooted.sh /mnt
cp -r ./conf /mnt/conf
cp -r ./sway /mnt/sway
arch-chroot /mnt /chrooted.sh ${rootPartition} ${user} ${kernelName} ${formatBoot} ${bootPartition} ${userPassword} ${rootPassword} ${desktop} ${gpu} ${hostname} ${portable} ${device} ${email} ${cpu} ${zen} ${winEfiPartition}
rm -f /mnt/chrooted.sh
rm -f /mnt/loged.sh
cp -f ./conf/mime.types /mnt/etc/mime.types
rm -rf /mnt/conf
rm -rf /mnt/sway
cat ./conf/original_example.sh > ./example.sh
printf "\nInstallation complete!\n"
