#!/bin/bash

declare -A sv
sv["mysql"]="db"
sv["postgres"]="db"

declare -A phs
while read line; do
    name=$(echo "${line}" | sed s/"|.*|.*$"/""/)
    container=$(echo ${line} | sed s/"[A-Za-z0-9]*|"/""/)
    container=$(echo ${container} | sed s/"|.*$"/""/)
    pth=$(echo "${line}" | sed s/".*|.*|"/""/)

    sv["${name}"]="${container}"
    phs["${name}"]="${pth}"
done < ~/.custom_images/paths.txt

if [ "${1}" = "start" ]; then
    if [ -z "${2}" ] || [ -z "${sv[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml start
elif [ "${1}" = "stop" ]; then
    if [ -z "${2}" ] || [ -z "${sv[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml stop
elif [ "${1}" = "status" ]; then
    if [ -z "${2}" ] || [ -z "${sv[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi
    lines=$(docker-compose ls --filter name=${2} | egrep "^mysql +running\(1\)" | wc -l)
    if [ ${lines} = "1" ]; then
        echo "Service ${2}\t $(tput bold)\033[0;33mactive$(tput sgr0)"
    elif [ ${lines} = "0" ]; then
        echo "Service ${2}\t $(tput bold)\033[0;31mstopped$(tput sgr0)"
    else
        echo "Invalid service name!"
    fi
elif [ "${1}" = "exec" ]; then
    if [ -z "${2}" ] || [ -z "${sv[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi
    c=""
    s=""
    for var in "$@"
    do
        if [ "$var" = "${2}" ]; then
            s="true"
            continue
        fi
        if [ "${s}" = "true" ]; then
            c="${c} ${var}"
        fi
    done
    if [ -z "${c}" ]; then
        echo "ERROR: no command given!"
        exit
    fi
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml exec "${sv[${2}]}" ${c}
elif [ "${1}" = "enter" ]; then
    if [ -z "${2}" ] || [ -z "${sv[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml exec ${sv["${2}"]} bash || \
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml exec ${sv["${2}"]} sh
elif [ "${1}" = "create" ]; then
    if [ -z "${2}" ]; then
        echo "ERROR: second parameter must be a name!"
        exit
    fi
    if  [ -z "${3}" ]; then
        echo "ERROR: third parameter must be a container name (from docker-compose.yml)!"
        exit
    fi
    if [ ! -z "${sv["${2}"]}" ]; then
        echo "ERROR: service named ${2} already exists!"
        exit
    fi
    if [ -z "${4}" ] || [[ ! -d "${4}" ]]; then
        echo "ERROR: Fourth parameter must be a valid directory!"
        exit
    fi
    mkdir ~/.custom_images/${2}
    cp -rf ${4}/* ~/.custom_images/${2}
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml build
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml create
    echo "${2}|${3}|$(readlink -m ${4})" >> ~/.custom_images/paths.txt
elif [ "${1}" = "update" ]; then
    if [ -z "${2}" ] || [ -z "${phs[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi

    if [ ! -z "${3}" ]; then
        ct="${sv["${2}"]}"
        sed -i s@"^${2}|${ct}|.*"@"${2}|${ct}|${3}|"@ ~/.custom_images/paths.txt
    fi

    cp -rf ${phs[${2}]}/* ~/.custom_images/${2}
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml build
    docker-compose -f ~/.custom_images/${2}/docker-compose.yml create
elif [ "${1}" = "remove" ] || [ "${1}" = "delete" ]; then
    if [ "${2}" = "postgres" ] || [ "${2}" = "mysql" ]; then
        echo "ERROR: service ${2} is built in. Cannot be removed."
        exit
    elif [ -z "${2}" ] || [ -z "${phs[${2}]}" ]; then
        echo "ERROR: invalid service name!"
        exit
    fi

    docker-compose -f ~/.custom_images/${2}/docker-compose.yml down
    sed -i "/^${2}|.*$/d" ~/.custom_images/paths.txt
    rm -rf ~/.custom_images/${2}
else
    echo "ERROR: invalid command ${1}!"
fi
