#!/bin/bash

./arch-install.sh \
--root-partition /dev/nvme0n1p2 \
--boot-partition /dev/nvme0n1p1 \
--home-partition /dev/nvme0n1p3 \
--user test \
--user-password test \
--user-email "email@email.com" \
--root-password test \
--format-boot \
--format-home \
--desktop gnome \
--gpu amd \
--cpu amd \
--zen-version 3 \
--hostname VIRTUAL \
--win-efi-partition /dev/nvme0n1p4 # optional - not set if pc does not contains windows os
