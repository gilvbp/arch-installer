#!/usr/bin/env python
from json import dumps
from i3ipc import Connection


if __name__ == "__main__":
	i3 = Connection()
	if i3.get_tree().find_focused():
		print(dumps({"text": "Closable", "percentage": 100}))
	else:
		print(dumps({"text": "Non-closable", "percentage": 0}))
