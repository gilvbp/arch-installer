output_name=$(swaymsg -t get_tree | jq -r '.nodes[] | select([recurse(.nodes[]?, .floating_nodes[]?) | .focused] | any) | .name')
res_x=$(swaymsg -t get_tree | egrep -E ".*name.*${output_name}" -A 30 | grep "\"rect\"" -A 5 | grep width | sed -r "s/.*: //" | sed "s/,//")

if [ ! $(swaymsg -t get_tree | grep -m1 "tilix-dropdown") ]; then
	nohup tilix --name "tilix-dropdown" --window-style borderless --new-process > /dev/null &
	sleep 0.4
fi
swaymsg scratchpad show


swaymsg "[app_id=\"tilix-dropdown\"] resize set width 85 ppt height 50 ppt"
x=$(swaymsg -t get_tree | grep "tilix-dropdown" -B 40 | grep "\"rect\"" -A 5 | grep "\"x\"" | sed -r "s/.*: //" | sed "s/,//")
y=$(swaymsg -t get_tree | grep "tilix-dropdown" -B 40 | grep "\"rect\"" -A 5 | grep "\"y\"" | sed -r "s/.*: //" | sed "s/,//")
w=$(swaymsg -t get_tree | grep "tilix-dropdown" -B 40 | grep "\"rect\"" -A 5 | grep "\"width\"" | sed -r "s/.*: //" | sed "s/,//")
h=$(swaymsg -t get_tree | grep "tilix-dropdown" -B 40 | grep "\"rect\"" -A 5 | grep "\"height\"" | sed -r "s/.*: //" | sed "s/,//")
swaymsg -t get_tree | egrep -E ".*name.*eDP-1" -A 30 | grep "\"rect\"" -A 5 | grep width | sed -r "s/.*: //" | sed "s/,//"


swaymsg "[app_id=\"tilix-dropdown\"] move absolute position $(( ($res_x-$w)/2 )) px 30 px"

