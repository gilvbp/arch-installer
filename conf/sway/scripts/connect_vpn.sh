ip route | grep -q ${2} && connected=1

if [ "${connected}" ]; then
	echo "Disable VPN"
	sudo killall openvpn
else
	echo "Enable VPN"
	sudo openvpn --config ${1} --data-ciphers BF-CBC --daemon
fi
