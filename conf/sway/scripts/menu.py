#!/usr/bin/env python
from os import system
from i3ipc import Connection
i3 = Connection()
tree = str(vars(i3.get_tree()))
print("menu_launcher" in tree)
if "menu_launcher" in tree or "network_launcher" in tree:
	i3.command("[app_id=\"menu_launcher\"] kill")
	i3.command("[app_id=\"network_launcher\"] kill")
else:
	system("foot --app-id=menu_launcher -e ~/.config/sway/scripts/sway-launcher-desktop.sh")
