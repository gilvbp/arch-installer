#!/bin/bash

mkdir ~/.config/wallpapers
mv ~/Pictures/lilith.png ~/.config/wallpapers

gnome-extensions enable vertical-overview@RensAlthuis.github.com

dconf load / < ~/.gnome.conf
rm -rf ~/.gnome.conf

gsettings set org.gnome.desktop.background picture-uri file:///home/${USER}/.config/wallpapers/lilith.png
gsettings set org.gnome.desktop.screensaver picture-uri file:///home/${USER}/.config/wallpapers/lilith.png

gsettings set org.gnome.shell.app-switcher current-workspace-only true
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['']"

sudo localectl set-x11-keymap cz
sudo localectl set-keymap cz

sudo sed -i s/"AutomaticLogin"/"# AutomaticLogin"/ /etc/gdm/custom.conf
sudo rm -rf /etc/sudoers2
sudo echo "%wheel ALL=(ALL) ALL" > ./sudoers2
sudo chown 0 ./sudoers2
sudo mv ./sudoers2 /etc/sudoers


# Docker db images prepare
cd ~/.custom_images/postgres
docker-compose build
docker-compose create
cd ~/.custom_images/mysql
docker-compose build
docker-compose create
cd ~



cd /tmp
nohup firefox --headless &> /dev/null &
sleep 4
killall firefox
ext_folder=(`echo ~/.mozilla/firefox/*.default-release/extensions/`)


get_addon_id_from_xpi () { #path to .xpi file
    addon_id_line=`unzip -p $1 install.rdf | egrep '<em:id>' -m 1`
    addon_id=`echo $addon_id_line | sed "s/.*>\(.*\)<.*/\1/"`
    echo "$addon_id"
}

get_addon_name_from_xpi () { #path to .xpi file
    addon_name_line=`unzip -p $1 install.rdf | egrep '<em:name>' -m 1`
    addon_name=`echo $addon_name_line | sed "s/.*>\(.*\)<.*/\1/"`
    echo "$addon_name"
}

install_addon () {
    xpi="${PWD}/${1}"
    extensions_path=$2
    new_filename=`get_addon_id_from_xpi $xpi`.xpi
    new_filepath="${extensions_path}${new_filename}"
    addon_name=`get_addon_name_from_xpi $xpi`
    if [ -f "$new_filepath" ]; then
        echo "File already exists: $new_filepath"
        echo "Skipping installation for addon $addon_name."
    else
        cp "$xpi" "$new_filepath"
    fi
}

wget https://addons.mozilla.org/firefox/downloads/file/4047353/ublock_origin-latest.xpi
install_addon ublock_origin-latest.xpi "$ext_folder"

wget https://addons.mozilla.org/firefox/downloads/file/3673761/shortkeys-latest.xpi
install_addon shortkeys-latest.xpi "$ext_folder"

wget https://addons.mozilla.org/firefox/downloads/file/4002882/tab_session_manager-latest.xpi
install_addon tab_session_manager-latest.xpi "$ext_folder"

wget https://addons.mozilla.org/firefox/downloads/file/4024031/facebook_container-latest.xpi
install_addon facebook_container-latest.xpi "$ext_folder"

wget https://addons.mozilla.org/firefox/downloads/file/4032375/modheader_firefox-latest.xpi
install_addon modheader_firefox-latest.xpi "$ext_folder"

wget https://addons.mozilla.org/firefox/downloads/file/4043507/i_dont_care_about_cookies-latest.xpi
install_addon i_dont_care_about_cookies-latest.xpi "$ext_folder"

#wget https://addons.mozilla.org/firefox/downloads/file/3518228/google_lighthouse-latest.xpi
#install_addon google_lighthouse-latest.xpi "$ext_folder"

cd

rm -rf /first_boot.sh
rm -rf ~/.config/autostart/first_boot-autostart.desktop
reboot
