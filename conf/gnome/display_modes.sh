#!/bin/bash

FN=`file ~/.config/pulse/*-default-sink`
pathSplit=(${FN//\// })
nameSplit=(${pathSplit[4]//-/ })
soundHash=${nameSplit[0]}

if [ "${1}" = "work" ]; then
        cp -rf ~/.config/monitors_work.xml ~/.config/monitors.xml
        cp -rf ~/.config/work_sink ~/.config/${soundHash}-default-sink
        gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
else
        cp -rf ~/.config/monitors_gaming.xml ~/.config/monitors.xml
        cp -rf ~/.config/gaming_sink ~/.config/${soundHash}-default-sink
        gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled false
fi

killall -3 gnome-shell
