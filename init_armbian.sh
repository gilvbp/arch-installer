#!/bin/sh

cdir=$(pwd)
u=${USER}

cp conf/armbian/bashrc ~/.bashrc
cp conf/armbian/profile ~/.profile
cp conf/armbian/dircolors ~/.dircolors
sudo cp conf/armbian/git /usr/share/bash-completion/completions/git

sudo apt update -y
sudo apt upgrade -y

sudo apt install htop psmisc git curl wget build-essential -y

mkdir ~/.ssh
echo "ForwardAgent yes" > ~/.ssh/config
echo "StrictHostKeyChecking no" >> ~/.ssh/config


## ROCK PI S LEDs:
## disable io led (blue blinking led)
# sudo su -c "echo 0 > /sys/class/leds/rockpis:blue:user/brightness"
## disable power led
## sudo su -c "echo 0 > /sys/class/leds/rockpis:green:power/brightness"


## Z WAVE GATEWAY SERVER
# sudo su -c "curl -fsSL https://deb.nodesource.com/setup_18.x | bash"
# sudo apt install nodejs
# npm config set ignore-scripts true
# sudo npm config set ignore-scripts true
# sudo npm install -g zwave-js @zwave-js/core @types/triple-beam @zwave-js/server @zwave-js/flash

# sudo cp conf/armbian/zwave-server.service /lib/systemd/system/zwave-server.service
# sudo cp conf/armbian/zwave-server.sh /usr/bin/zwave-server.sh
# sudo chmod +x /usr/bin/zwave-server.sh
# sudo systemctl enable zwave-server.service
# sudo systemctl start zwave-server.service


# NGINX GATEWAY
# TODO


# GITLAB
# TODO


# DOCKER & GITAB RUNNER
# sudo apt-get install ca-certificates gnupg lsb-release -y
# sudo mkdir -p /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
# echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
#   $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# sudo chmod a+r /etc/apt/keyrings/docker.gpg
# sudo apt-get update
# sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin apparmor
# sudo gpasswd -a ${u} docker
# sudo systemctl enable docker.service
# sudo systemctl start docker.service
# curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# sudo apt-get install gitlab-runner
## sudo gitlab-runner register


# TERRARIA SERVER
# sudo apt-get install mono-complete screen
# cd
# wget https://terraria.org/api/download/pc-dedicated-server/terraria-server-1449.zip
# unzip terraria-server*
# cd ${cdir}
# sudo cp conf/armbian/terraria-server.service /lib/systemd/system/terraria-server.service
# sudo cp conf/armbian/terraria-server.sh /usr/bin/terraria-server.sh
# sudo chmod +x /usr/bin/terraria-server.sh
# sudo chmod a+w /var/log
# ln -s /home/${USER}/1449/Linux/ /home/${USER}/Terraria-server
# sudo systemctl enable terraria-server.service

### generate world
## mono --server --gc=sgen -O=all /home/${USER}/Terraria-server/TerrariaServer.exe \
## -port 7777 \
## -players 16 \
## -pass P@ssW0rd


# MINECRAFT SERVER
# TODO


cd /usr/bin
sudo ln -sf python3 python
cd

sudo apt autoremove
