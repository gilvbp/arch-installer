#!/bin/sh

cd ~
mkdir Work
mkdir Downloads
mkdir Documents
mkdir Experiments
mkdir Music
mkdir Pictures
mkdir Projects
mkdir .custom_images
mkdir .config/gtk-4.0

cp -rf /conf/custom_images/* ./.custom_images/

git config --global core.editor nano
git config --global user.email "${3}"
git config --global user.name "${2}"

if [ "${1}" = "gnome" ]; then
    cp -rf /conf/gnome/display_modes.sh ~/.config/display_modes.sh
    cp -rf /conf/gnome/monitors_gaming.xml ~/.config/monitors_gaming.xml
    cp -rf /conf/gnome/monitors_work.xml ~/.config/monitors_work.xml
    cp -rf /conf/gnome/gaming_sink ~/.config/gaming_sink
    cp -rf /conf/gnome/work_sink ~/.config/work_sink
    mkdir ~/.mozilla
    cp -rf /conf/firefox ~/.mozilla/firefox
fi


cp -rf /conf/lilith.png ~/Pictures/
cp -rf /conf/dircolors ~/.dircolors


mkdir -p ~/.config/Code\ -\ OSS/User/
code --install-extension saulecabrera.asls
code --install-extension formulahendry.auto-close-tag
code --install-extension CoenraadS.bracket-pair-colorizer-2
code --install-extension anseki.vscode-color
code --install-extension ms-azuretools.vscode-docker
code --install-extension EditorConfig.EditorConfig
code --install-extension dbaeumer.vscode-eslint
code --install-extension waderyan.gitblame
code --install-extension ecmel.vscode-html-css
code --install-extension iocave.monkey-patch
code --install-extension ms-python.python
code --install-extension mechatroner.rainbow-csv
code --install-extension rust-lang.rust
code --install-extension Gruntfuggly.todo-tree
code --install-extension gengjiawen.vscode-wasm
code --install-extension ms-toolsai.jupyter
code --install-extension akamud.vscode-theme-onedark
code --install-extension bungcip.better-toml
code --install-extension mpty.pack-arduino
code --install-extension MAKinteract.micro-bit-python
code --install-extension espressif.esp-idf-extension
code --install-extension ms-python.devicesimulatorexpress
cp -rf /conf/vscode/* ~/.config/Code\ -\ OSS/User/

npm config set ignore-scripts true

mkdir ~/.config/sublime-text
cp -rf /conf/sublime/* ~/.config/sublime-text/
alias yay='yay --mflags --skipinteg --cleanafter --removemake  --noconfirm'

cd /tmp
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions

yay -S authbind
yay -S ferdium-bin notion-app

yay -S wasmtime-bin binaryen-git assemblyscript
yay -S arduino-ide-bin
yay -S sublime-text-4

yay -S heroku-cli

yay -S minecraft-launcher

if [ "${1}" = "sway" ]; then
    yay -S greetd greetd-gtkgreet
    yay -S redshift-wayland-git
    mkdir ~/.mozilla
    cp -rf /conf/firefox ~/.mozilla/firefox
    pip install pywayland pywlroots i3ipc setproctitle evdev psutil
fi

if [ "${4}" = "amd" ]; then
    if [ "${5}" = "2" ]; then
        yay -S zenmonitor
    else
        yay -S zenmonitor3-git
    fi
    yay -S zenpower-dkms
    yay -S ryzenadj-git
    yay -S ryzen-controller-bin
    yay -S amdctl
fi

cd
/opt/kite/kite-installer install

git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions

cd /tmp
git clone https://github.com/RensAlthuis/vertical-overview.git
cd vertical-overview
make
make install

